## Sign Up Landing Page

A static site to practice HTML. View [Demo](https://iruldanet.gitlab.io/signup-landing/).

![HTML (Jade) & CSS (SASS) static sign up form](/images/signup-landing.png)

